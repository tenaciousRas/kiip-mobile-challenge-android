This is the source code for Part 1 of the Kiip Mobile Challenge.  This provides a HackerNewsSDK with the specified methods:
initSDK(Context)
showHackerNews()

The SDK also provides notifier and listener interfaces for listening to initliazation events.

To build the library, build using Android Studio 2.3 or higher.  Alternatively you can use Gradle v3.2 or higher to build from CLI
with Gradle.

This IntelliJ project provides the hackernewssdk library (/hackernewssdk) and an Android application (/app) that provides a demonstration
of the library in action.
