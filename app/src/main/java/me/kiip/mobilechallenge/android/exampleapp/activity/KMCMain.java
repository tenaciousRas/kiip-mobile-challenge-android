package me.kiip.mobilechallenge.android.exampleapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import me.kiip.hackernewssdk.android.HackerNewsSDK;
import me.kiip.mobilechallenge.android.exampleapp.R;

/**
 * Created by freeb on 3/9/17.
 */

public class KMCMain extends AppCompatActivity {

    public static final String BLANK = "";

    private Button mButton;
    private TextView mLoadingText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        final HackerNewsSDK hn = HackerNewsSDK.getInstance();
        hn.addSDKInitListener(new HackerNewsSDK.SDKInitListener() {
            @Override
            public void onInitError() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLoadingText.setText(getString(R.string.txt_la_error));
                    }
                });
            }

            @Override
            public void onInitStart() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLoadingText.setText(getString(R.string.txt_la_starting));
                    }
                });
            }

            @Override
            public void onInitProgress(final int progress) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLoadingText.setText(getString(R.string.txt_la)
                                + String.format("%3.2f", ((float) progress / 1024f * 100f))
                                + getString(R.string.txt_percent_complete));
                    }
                });
            }

            @Override
            public void onInitComplete() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLoadingText.setText(BLANK);
                        mButton.setEnabled(true);
                    }
                });
            }
        });
        mLoadingText = (TextView) findViewById(R.id.loadingTextView);
        mButton = (Button) findViewById(R.id.button);
        hn.initSDK(KMCMain.this);
        mButton.setEnabled(hn.isArticlesLoaded());
        mButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                hn.showHackerNews();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        final HackerNewsSDK hn = HackerNewsSDK.getInstance();
        hn.onDestroy();
    }
}
