package me.kiip.hackernewssdk.android;

/**
 * Created by freeb on 3/9/17.
 */

public interface SDKInitNotifier {

    /**
     * Add a SDK init listener.
     *
     * @param listener listener to add
     */
    public void addSDKInitListener(HackerNewsSDK.SDKInitListener listener);

    /**
     * Remove a SDK init listener.
     *
     * @param listener listener to remove
     */
    public void deleteSDKInitListener(HackerNewsSDK.SDKInitListener listener);
}
