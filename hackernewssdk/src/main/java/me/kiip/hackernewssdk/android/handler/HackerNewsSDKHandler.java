package me.kiip.hackernewssdk.android.handler;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import me.kiip.hackernewssdk.android.Constants;
import me.kiip.hackernewssdk.android.HackerNewsSDK;

/**
 * Created by freeb on 3/9/17.
 */
public class HackerNewsSDKHandler extends Handler {
    public static final String TAG = "HackerNewsSDKHandler";

    public static final int MSG_LOAD_TOP_20_ARTICLES = 10;
    public static final int MSG_LOAD_ARTICLE = 20;
    public static final int PROG_10BIT_STEP_SIZE = 1024 / 21;
    public static final int MAX_SIZE_ARTICLE_LIST = 20;

    private WeakReference<HackerNewsSDK> sdk;
    private boolean articlesLoaded;
    private JSONArray top20ArticleIds;

    public HackerNewsSDKHandler(Looper looper, WeakReference<HackerNewsSDK> sdk) throws IllegalArgumentException {
        super(looper);
        if (sdk == null) {
            throw new IllegalArgumentException("sdk cannot be null");
        }
        this.sdk = sdk;
        this.articlesLoaded = false;
    }

    /**
     * Set the SDK reference.
     * @param sdk SDK reference to use.
     */
    public void setSdk(WeakReference<HackerNewsSDK> sdk) {
        this.sdk = sdk;
    }

    /**
     * @return true if Top20 articles are completely loaded, false otherwise
     */
    public boolean isArticlesLoaded() {
        return articlesLoaded;
    }

    /**
     * @return cached Trop20 hackernews articles
     */
    public List<JSONObject> getArticles() {
        return this.sdk.get().getArticles();
    }

    @Override
    public void handleMessage(Message msg) {
        super.handleMessage(msg);
        switch (msg.what) {
            case MSG_LOAD_TOP_20_ARTICLES:
                articlesLoaded = false;
                getArticles().clear();
                top20ArticleIds = loadHackerNewsTop();
                sdk.get().notifyInitSDKProgress(PROG_10BIT_STEP_SIZE);
                break;
            case MSG_LOAD_ARTICLE:
                Bundle data = msg.getData();
                int index = data.getInt(Constants.BUNDLE_KEY_ARTICLE_IDX);
                String articleId = data.getString(Constants.BUNDLE_KEY_ARTICLE_ID);
                getArticles().add(index, loadHackerNewsArticle(articleId));
                sdk.get().notifyInitSDKProgress(PROG_10BIT_STEP_SIZE * (index + 2));
                if (getArticles().size() == top20ArticleIds.length()) {
                    articlesLoaded = true;
                    sdk.get().notifyInitSDKComplete();
                }
            default:
                break;
        }
    }

    /**
     * Load Top20 HackerNews article IDs.
     * Only the IDs are returned from the call to @{@link Constants#HTTPS_HACKER_NEWS_FIREBASEIO_COM_V0_NEWSTORIES_JSON}.
     *
     * @return JSONArray of article IDs
     */
    protected JSONArray loadHackerNewsTop() {
        JSONArray ret = null;
        try {
            String topNewsJSON = doRESTGet(Constants.HTTPS_HACKER_NEWS_FIREBASEIO_COM_V0_NEWSTORIES_JSON);
            if (topNewsJSON == null || topNewsJSON.length() < 1) {
                ret = new JSONArray();
            } else {
                ret = new JSONArray(topNewsJSON);
            }
            while (ret.length() > MAX_SIZE_ARTICLE_LIST) {
                ret.remove(ret.length() - 1);
            }
            Message msg = null;
            Bundle data = null;
            for (int i = 0; i < ret.length(); i++) {
                msg = this.obtainMessage(MSG_LOAD_ARTICLE);
                data = new Bundle();
                data.putInt(Constants.BUNDLE_KEY_ARTICLE_IDX, i);
                data.putString(Constants.BUNDLE_KEY_ARTICLE_ID, ret.getString(i));
                msg.setData(data);
                this.sendMessage(msg);
            }
        } catch (JSONException e) {
            Log.d(TAG, Log.getStackTraceString(e));
        }
        return ret;
    }

    /**
     * Load a HackerNews article.
     *
     * @param articleId
     * @return JSON from the REST API call to
     */
    protected JSONObject loadHackerNewsArticle(final String articleId) {
        String aId = "";
        if (null != articleId) {
            aId = articleId;
        }
        String url = Constants.HTTPS_HACKER_NEWS_FIREBASEIO_COM_V0_ITEM_BASE + aId + Constants.EXT_JSON;
        JSONObject ret = null;
        try {
            String articleJSON = doRESTGet(url);
            ret = new JSONObject(articleJSON == null ? "" : articleJSON);
        } catch (JSONException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return ret;
    }

    /**
     * Convert an InputStream to a String.
     *
     * @param in InputStream to convert.
     * @return in as a String, or null of in==null
     */
    protected String convertInputStream(InputStream in) {
        BufferedReader streamReader = null;
        StringBuilder responseStrBuilder = new StringBuilder();
        String inputStr;
        try {
            streamReader = new BufferedReader(new InputStreamReader(in, Constants.ENC_UTF8));
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return responseStrBuilder.toString();
    }

    /**
     * Call a REST endpoint with given URL.
     * @param url REST endpoint to fetch
     * @return GET response body as a string
     */
    protected String doRESTGet(String url) {
        if (!this.sdk.get().isNetworkAvailable(null)) {
            Log.w(TAG, "network unavailable, unable to GET from " + url);
            this.sdk.get().notifyInitSDKError();
            return null;
        }
        InputStream in = null;
        String ret = null;
        URL lUrl = null;
        HttpURLConnection urlConnection = null;
        try {
            lUrl = new URL(url);
            urlConnection = (HttpURLConnection) lUrl.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream());
            ret = convertInputStream(in);
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        } finally {
            urlConnection.disconnect();
        }
        return ret;
    }
}
