package me.kiip.hackernewssdk.android.adapter;

/**
 * Created by freeb on 3/9/17.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import me.kiip.hackernewssdk.android.Constants;
import me.kiip.hackernewssdk.android.R;

public class HackerNewsListItemAdapter extends BaseAdapter {
    public static final String TAG = "HNListItemAdapter";

    /**
     * Container for the views displayed in each row.
     */
    private class ViewHolder {
        TextView title;
        TextView author;
        TextView score;
        TextView time;
    }

    private Context mContext;
    private LayoutInflater mInflater;
    private List<JSONObject> mData;
    private SimpleDateFormat sdf;

    public HackerNewsListItemAdapter(Context context, List<JSONObject> items) {
        mContext = context;
        mData = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sdf = new SimpleDateFormat();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.hackernews_item, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.titleTextView);
            holder.author = (TextView) convertView.findViewById(R.id.authorTextView);
            holder.score = (TextView) convertView.findViewById(R.id.scoreTextView);
            holder.time = (TextView) convertView.findViewById(R.id.timeTextView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        JSONObject item = (JSONObject) getItem(position);
        try {
            holder.title.setText(item.getString("title"));
            holder.author.setText(mContext.getString(R.string.label_author) + Constants.COLON_SPACE + item.getString(Constants.JSON_KEY_ARTICLE_AUTHOR));
            holder.score.setText(mContext.getString(R.string.label_score) + Constants.COLON_SPACE + item.getString(Constants.JSON_KEY_ARTICLE_SCORE) + Constants.BLANK);
            holder.time.setText(sdf.format(new Date(Long.parseLong(item.getString(Constants.JSON_JEY_ARTICLE_TIME)) * 1000)) + Constants.BLANK);
        } catch (JSONException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return convertView;
    }
}