package me.kiip.hackernewssdk.android;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import me.kiip.hackernewssdk.android.handler.HackerNewsSDKHandler;
import me.kiip.hackernewssdk.android.view.HackerNewsDialog;

/**
 * Created by freeb on 3/9/17.
 * SDK for the HackerNews REST API at https://github.com/HackerNews/API.
 */

public class HackerNewsSDK implements SDKInitNotifier {

    public static final String TAG = "HackerNewsSDK";

    public static interface SDKInitListener {
        /**
         * Called when SDK initialization throws an error.
         */
        public void onInitError();

        /**
         * Called when SDK initialization starts.
         */
        public void onInitStart();

        /**
         * Called during SDK initialization progress.
         *
         * @param progress 10-bit completion progress (0-1024)
         */
        public void onInitProgress(int progress);

        /**
         * Called when SDK initialization completes.
         */
        public void onInitComplete();
    }

    private static HackerNewsSDK instance;
    private HackerNewsSDKHandler mTaskHandler;
    private HackerNewsDialog dialog;
    private Thread mTaskHandlerThread;
    private Set<SDKInitListener> listeners;
    private List<JSONObject> articles;
    private Context mCtx;

    private HackerNewsSDK() {
        if (listeners == null) {
            listeners = new HashSet<SDKInitListener>();
        }
    }

    /**
     * Get singleton SDK instance.
     * @return this instance
     */
    public static HackerNewsSDK getInstance() {
        if (instance == null) {
            instance = new HackerNewsSDK();
        }
        return instance;
    }

    /**
     *
     * @return class context
     */
    public Context getmCtx() {
        return mCtx;
    }

    /**
     *
     * @return message handler assigned to main thread
     */
    public HackerNewsSDKHandler getMTaskHandler() {
        return mTaskHandler;
    }

    /**
     *
     * @return @{@link this#dialog}
     */
    public HackerNewsDialog getDialog() {
        return dialog;
    }

    /**
     *
     * @param articles list of articles to set
     */
    public void setArticles(List<JSONObject> articles) {
        this.articles = articles;
    }

    /**
     *
     * @return list of retrieved articles
     */
    public List<JSONObject> getArticles() {
        return articles;
    }

    /**
     * Initialize the SDK.  Loads the top 20 articles from the REST API.
     *
     * @param ctx application/activity context used for networking and display
     */
    public void initSDK(final Context ctx) {
        getInstance();
        dialog = new HackerNewsDialog();
        articles = new ArrayList<JSONObject>();
        dialog.setHackerNewsItems(articles);
        this.mCtx = ctx;
        initHandlerThread();
        Message msg = mTaskHandler.obtainMessage(HackerNewsSDKHandler.MSG_LOAD_TOP_20_ARTICLES);
        mTaskHandler.sendMessage(msg);
        notifyInitSDKStart();
    }

    /**
     * Garbage collection.
     */
    public void onDestroy() {
        if (mTaskHandler != null) {
            mTaskHandler.removeCallbacksAndMessages(null);
            mTaskHandler.getLooper().quit();
        }
        if (mTaskHandlerThread != null && !mTaskHandlerThread.isInterrupted()) {
            mTaskHandlerThread.interrupt();
        }
        mTaskHandler = null;
    }

    /**
     * Convenience method for @{@link HackerNewsSDK#showHackerNews(Context)}
     * using context given at init.
     */
    public void showHackerNews() {
        showHackerNews(null);
    }

    /**
     * Display HackerNews Top20 in a dialog.
     *
     * @param ctx display context to use, or use the one given at init
     */
    public void showHackerNews(final Context ctx) {
        Context lCtx = buildLocalOrClassContext(ctx);
        if (lCtx == null) {
            Log.e(TAG, "Could not show hackernews dialog due to a missing context item.");
            return;
        }
        Activity lAct = (Activity) lCtx;
        dialog.show(lAct.getFragmentManager(), lCtx.getString(R.string.app_name));
    }

    @Override
    public void addSDKInitListener(SDKInitListener listener) {
        if (listeners.contains(listener)) {
            return;
        }
        listeners.add(listener);
    }

    @Override
    public void deleteSDKInitListener(SDKInitListener listener) {
        listeners.remove(listener);
    }

    /**
     * Notify all SDK listeners of SDK init error event.
     */
    public void notifyInitSDKError() {
        if (dialog.getmListAdapter() != null) {
            ((Activity) dialog.getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.getmListAdapter().notifyDataSetChanged();
                }
            });
        }
        if (listeners != null) {
            for (SDKInitListener lst : listeners) {
                lst.onInitError();
            }
        }
    }

    /**
     * Notify all SDK listeners of SDK init start event.
     */
    public void notifyInitSDKStart() {
        if (dialog.getmListAdapter() != null) {
            ((Activity) dialog.getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.getmListAdapter().notifyDataSetChanged();
                }
            });
        }
        if (listeners != null) {
            for (SDKInitListener lst : listeners) {
                lst.onInitStart();
            }
        }
    }

    /**
     * Notify all SDK listeners of SDK init progress event.
     *
     * @param progress
     */
    public void notifyInitSDKProgress(int progress) {
        if (dialog.getmListAdapter() != null) {
            ((Activity) dialog.getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.getmListAdapter().notifyDataSetChanged();
                }
            });
        }
        if (listeners != null) {
            for (SDKInitListener lst : listeners) {
                lst.onInitProgress(progress);
            }
        }
    }

    /**
     * Notify all SDK listeners of SDK init complete event.
     */
    public void notifyInitSDKComplete() {
        if (dialog.getmListAdapter() != null) {
            ((Activity) dialog.getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    dialog.getmListAdapter().notifyDataSetChanged();
                }
            });
        }
        if (listeners != null) {
            for (SDKInitListener lst : listeners) {
                lst.onInitComplete();
            }
        }
    }

    /**
     * Toast message if network not available.
     *
     * @param ctx context for @{@link Context#getSystemService(Class)}
     */
    public void showNetworkNotAvailable(final Context ctx) {
        Context lCtx = buildLocalOrClassContext(ctx);
        if (lCtx == null) {
            Log.e(TAG, "Could not toast network availability due to a missing context item.");
            return;
        }
        Toast.makeText(lCtx, Constants.TOAST_TXT_NET_PROB,
                Toast.LENGTH_LONG).show();
    }

    /**
     * Check if network is available.
     *
     * @param ctx context for @{@link Context#getSystemService(Class)}
     * @return true if network is available at time of call, false otherwise
     */
    public boolean isNetworkAvailable(final Context ctx) {
        Context lCtx = buildLocalOrClassContext(ctx);
        boolean isAvailable = false;
        ConnectivityManager manager = (ConnectivityManager)
                lCtx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            isAvailable = true;
        }
        return isAvailable;
    }

    /**
     *
     * @return true if handler loaded all articles, false otherwise
     */
    public boolean isArticlesLoaded() {
        if (mTaskHandler == null) {
            return false;
        }
        return mTaskHandler.isArticlesLoaded();
    }

    /**
     * start handler thread.
     */
    protected void initHandlerThread() {
        mTaskHandlerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                mTaskHandler = new HackerNewsSDKHandler(Looper.myLooper(), new WeakReference<HackerNewsSDK>(instance));
                Looper.loop();
            }
        });
        mTaskHandlerThread.start();
        // wait for mTaskHandler to initialize
        while (mTaskHandler == null) {
            Log.i(TAG, "init SDK handler thread...");
            continue;
        }
        Log.i(TAG, "handler thread initalized, handler=" + mTaskHandler.toString());
    }

    /**
     * @param ctx local class context
     * @return ctx if not null, otherwise @{@link this#mCtx}
     */
    private Context buildLocalOrClassContext(final Context ctx) {
        if (ctx == null) {
            return this.mCtx;
        }
        return ctx;
    }

}
