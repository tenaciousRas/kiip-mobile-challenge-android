package me.kiip.hackernewssdk.android.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import me.kiip.hackernewssdk.android.Constants;
import me.kiip.hackernewssdk.android.R;
import me.kiip.hackernewssdk.android.adapter.HackerNewsListItemAdapter;


/**
 * Created by freeb on 3/9/17.
 */

public class HackerNewsDialog extends DialogFragment {
    public static final String TAG = "HackerNewsDialog";

    private HackerNewsListItemAdapter mListAdapter;
    private List<JSONObject> mItems;
    private ListView mListView;

    /**
     *
     * @return list adapter used by this dialog
     */
    public HackerNewsListItemAdapter getmListAdapter() {
        return mListAdapter;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.hackernews_list, null);
        mListView = (ListView) v.findViewById(R.id.hackerNewsListView);
        mListAdapter = new HackerNewsListItemAdapter(getActivity(), mItems);
        mListView.setAdapter(mListAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String url = null;
                try {
                    url = mItems.get(position).getString(Constants.JSON_KEY_ARTICLE_URL);
                    if (url == null) {
                        Toast.makeText(getActivity(), getString(R.string.txt_article_has_no_link), Toast.LENGTH_SHORT).show();
                    } else {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
                Log.d(TAG, "onItemClick: " + url);
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(v);
        return builder.create();
    }

    public void setHackerNewsItems(List<JSONObject> items) {
        mItems = items;
    }

}
