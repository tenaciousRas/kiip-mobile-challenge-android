package me.kiip.hackernewssdk.android;

/**
 * Created by freeb on 3/11/17.
 */

public class Constants {
    public static final String BLANK = "";
    public static final String COLON_SPACE = ": ";
    public static final String EXT_JSON = ".json";
    public static final String ENC_UTF8 = "UTF-8";
    public static final String BUNDLE_KEY_ARTICLE_IDX = "articleIndex";
    public static final String BUNDLE_KEY_ARTICLE_ID = "articleId";
    public static final String JSON_KEY_ARTICLE_URL = "url";
    public static final String JSON_KEY_ARTICLE_AUTHOR = "by";
    public static final String JSON_KEY_ARTICLE_SCORE = "score";
    public static final String JSON_JEY_ARTICLE_TIME = "time";
    public static final String HTTPS_HACKER_NEWS_FIREBASEIO_COM_V0_NEWSTORIES_JSON = "https://hacker-news.firebaseio.com/v0/newstories.json";
    public static final String HTTPS_HACKER_NEWS_FIREBASEIO_COM_V0_ITEM_BASE = "https://hacker-news.firebaseio.com/v0/item/";
    public static final String TOAST_TXT_NET_PROB = "A Network Problem Occurred";
}
